# ARTFX META INC

_**Object Embedded Payments in Virtual Space.**_

ARTFX META INC Constructs the digital metaverse platform ARTFX; pronounced, 'Artifacts', the platform is focused on eCommerce innovations and augmented digital services for individual merchant-artists by creating exclusive interoperable metaverse spaces that function as hybrid art marketplaces and webstores. Within these digital spaces, virtual objects exist with payment metadata embedded so users and objetcs can stream payments bilaterally.
