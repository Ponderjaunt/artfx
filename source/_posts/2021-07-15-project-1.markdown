---
title: ARTFX Torus Demo
subtitle: Streaming Payments With Object Interactions
layout: default
modal-id: 1
date: 2021-07-15
img: roundicons.png
thumbnail: roundicons-thumbnail.png
alt: image-alt
project-date: June 2021
client: ARTFX
category: Web Development / Payments / Virtual Reality / Demo 
description:
ARTFX enables payments to be embeded within objects, allowing for object level interaction based transactions. Interact with the Torus knot to stream payments directly to payment pointers that belong to individuals belonging to the larger community of web 3.0 developers. 
---
